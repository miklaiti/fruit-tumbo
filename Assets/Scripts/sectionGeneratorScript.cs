﻿using UnityEngine;
using System.Collections;

public class sectionGeneratorScript : MonoBehaviour {
    public Transform generationPoint;
    public GameObject[] sections;
    private int sectionIndex;
    public cameraController cam;

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void generateSection()
    {
        transform.position = new Vector3(transform.position.x, generationPoint.transform.position.y, transform.position.z);
        sectionIndex = Random.Range(0, sections.Length);
        Instantiate(sections[sectionIndex], new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z), transform.rotation);
    }
    /*
        public void generateSection() {
           
        if (cam.cameraMovedToPlace) {
            transform.position = new Vector3(transform.position.x, generationPoint.transform.position.y, transform.position.z);
            sectionIndex = Random.Range(0, sections.Length);
            clonedSec = (GameObject) Instantiate(sections[sectionIndex], new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z), transform.rotation);
        }
    }
    */
}


