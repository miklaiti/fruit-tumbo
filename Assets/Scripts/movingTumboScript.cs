﻿using UnityEngine;
using System.Collections;

public class movingTumboScript : MonoBehaviour {
    public float speed;
    private Animator playerAnimator;
    private Rigidbody2D myRigidbody;
    private GameObject[] tumboSpawn;
    private int tumboSpawnIndex;
    private bool goingRight;
    private int willTumboStop;
    private int whichWayIfStopped;
    private float TimeCounter;
    // Use this for initialization
    void Start() {

        myRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        tumboSpawn = GameObject.FindGameObjectsWithTag("tumboSpawn");

        spawnAndMoveTumbo();

    }

    // Update is called once per frame
    void Update() {


        if (goingRight)
            {
            if (transform.position.x >= 0)
            {
                willTumboStop = 1;
                if (willTumboStop == 1)
                {  
                    myRigidbody.velocity = new Vector3(0, 0, 0);
                   /* Invoke("turnTumboLeft", 1);
                    Invoke("turnTumboRight", 2);
                    Invoke("moveAway", 3);
                */    
                
                    TimeCounter -= Time.deltaTime;

                    if (TimeCounter <= 2)
                    {
                        turnTumboLeft();
                    }
                    
                    if (TimeCounter <= 1)
                    {
                        turnTumboRight();
                    }

                    if (TimeCounter <= 0)
                    {
                        moveAway();
                    }
                    
                }
            }
            }

            else if (goingRight == false)
            {
                if (transform.position.x <= 0)
                {
                willTumboStop = 1;
                if (willTumboStop == 1)
                {
                    
                    myRigidbody.velocity = new Vector3(0, 0, 0);
                    /*
                    Invoke("turnTumboRight", 1);
                    Invoke("turnTumboLeft",2);
                    Invoke("moveAway", 3);
                    */
                    TimeCounter -= Time.deltaTime;

                    if (TimeCounter <= 2) {
                        turnTumboRight();
                    }

                    if (TimeCounter <= 1)
                    {
                        turnTumboLeft();
                    }

                    if (TimeCounter <= 0) {
                        moveAway();
                    }
                }
            }
            }

            playerAnimator.SetFloat("speed", Mathf.Abs(myRigidbody.velocity.x));
        
    }

    void turnTumboLeft() {
        transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
    }

    void turnTumboRight()
    {
        transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
    }

    void moveAway() {       
        if (whichWayIfStopped == 0)
        {
            turnTumboLeft();
        myRigidbody.velocity = new Vector3(-1, 0, 0);           
        }
        else
        {
            turnTumboRight();
         myRigidbody.velocity = new Vector3(1, 0, 0);           
        }
        
        }

    void spawnAndMoveTumbo()
    {
        TimeCounter = 3;
        tumboSpawnIndex = Random.Range(0, tumboSpawn.Length);
        this.transform.position = tumboSpawn[tumboSpawnIndex].transform.position;
        willTumboStop = Random.Range(0, 2);
        whichWayIfStopped = Random.Range(0, 2);
      
        if (tumboSpawnIndex == 0)
        {
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
            myRigidbody.velocity = new Vector3(1, 0, 0);
            this.transform.localScale = new Vector3(2, 2, 0);
            goingRight = true;
        }
        else if (tumboSpawnIndex == 1)
        {
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
            myRigidbody.velocity = new Vector3(1, 0, 0);     
            this.transform.localScale = new Vector3(1, 1, 0);
            goingRight = true;
        }

        else if (tumboSpawnIndex == 2)
        {
            myRigidbody.velocity = new Vector3(-1, 0, 0);
            transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
            this.transform.localScale = new Vector3(1, 1, 0);
            goingRight = false;
        }
        else if (tumboSpawnIndex == 3) {
            myRigidbody.velocity = new Vector3(-1, 0, 0);
            transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
            this.transform.localScale = new Vector3(2, 2, 0);
            goingRight = false;
        }


      
        playerAnimator.SetFloat("speed", Mathf.Abs(myRigidbody.velocity.x));       
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "tumboSpawn")
        {
            spawnAndMoveTumbo();
        }
    }

}