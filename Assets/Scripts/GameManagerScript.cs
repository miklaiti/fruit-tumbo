﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour
{
    private Vector3 playerStartPoint;
    public PlayerControllerScript thePlayer;
    public cameraController cam;
    //private ScoreManager scoreManager;

    // Use this for initialization
    void Start()
    {
        //scoreManager = ScoreManager.FindObjectOfType<ScoreManager>();
        playerStartPoint = thePlayer.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void restartGame()
    {

        StartCoroutine("RestartGameCo");
    }



    public IEnumerator RestartGameCo()
    {
        //scoreManager.scoreIncreasing = false;
       
        thePlayer.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);

        thePlayer.transform.position = playerStartPoint;
        thePlayer.gameObject.SetActive(true);
        cam.resetCamera();
    }
}
