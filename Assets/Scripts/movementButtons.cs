﻿using UnityEngine;
using System.Collections;


public class movementButtons : MonoBehaviour {
    private bool facingRight;
    RectTransform movementButton;
    public PlayerControllerScript thePlayer;
    // Use this for initialization
    void Start () {
        movementButton = gameObject.GetComponent<RectTransform>();
        facingRight = true;  
    }

    void FixedUpdate()
    {
        // Kuinka monta sormea koskee näyttöön?
        int tCount = Input.touchCount;
        // Enemmän kuin nolla
        if (tCount > 0)
        {
            for (int i = 0; i < tCount; i++)
            {
                Touch touch = Input.GetTouch(i);               
                Ray screenRay = Camera.main.ScreenPointToRay(touch.position);               
                RaycastHit hit;
                if (Physics.Raycast(screenRay, out hit))
                {                    
                    switch (hit.collider.gameObject.name)
                    {
                        case "moveleft":
                            moveLeft();
                            break;
                        case "moveright":
                            moveRight();
                            break;
                        case "jump":
                            jump();
                            break;
                    }
                }                
            }
        }
       

        float horizontal = Input.GetAxis("Horizontal");
        if (Input.GetKey(KeyCode.Space))
        {
            flip(horizontal);
            thePlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(0, thePlayer.jumpHeight);
        }

        if (Input.GetKey(KeyCode.A))
        {
            flip(horizontal);
            thePlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(-thePlayer.moveSpeed, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            flip(horizontal);
            thePlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(thePlayer.moveSpeed, 0);
        }


    }

    void moveLeft()
    {
        float horizontal = Input.GetAxis("Horizontal");
        flip(1);
        thePlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(- thePlayer.moveSpeed, thePlayer.GetComponent<Rigidbody2D>().velocity.y);
        Debug.Log(thePlayer.GetComponent<Rigidbody2D>().velocity);
    }
    void moveRight()
    {
        float horizontal = Input.GetAxis("Horizontal");
        flip(-1);
        thePlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(thePlayer.moveSpeed, thePlayer.GetComponent<Rigidbody2D>().velocity.y);
        Debug.Log(thePlayer.GetComponent<Rigidbody2D>().velocity);
    }

    void jump()
    {
        float horizontal = Input.GetAxis("Horizontal");
        flip(horizontal);
        if (thePlayer.jumpTimeCounter > 0)
        {
            thePlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(thePlayer.GetComponent<Rigidbody2D>().velocity.x, thePlayer.jumpHeight);
            thePlayer.jumpTimeCounter -= Time.deltaTime;
        }
        }
    

    private void flip(float horizontal)
    {
        if (horizontal < 0 && !facingRight || horizontal > 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = thePlayer.transform.localScale;
            theScale.x *= -1;
            thePlayer.transform.localScale = theScale;
        }
    }
}
