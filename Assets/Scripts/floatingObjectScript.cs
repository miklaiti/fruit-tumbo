﻿using UnityEngine;
using System.Collections;

public class floatingObjectScript : MonoBehaviour {
    Vector3 floatY;
    float OriginalY;
    public float FloatStrength; // Set strength in Unity


    // Use this for initialization
    void Start () {
        OriginalY = this.transform.position.y;
	}

    void Update()
    {
        floatY = transform.position;
        floatY.y = OriginalY + (Mathf.Sin(Time.time) * FloatStrength);
        transform.position = floatY;
    }
}
