﻿using UnityEngine;
using System.Collections;

public class spawnFruitScript : MonoBehaviour {

    private static GameObject [] fruits;
    private static GameObject[] spawnpoints;
    static int fruitIndex;
    static int spawnpointIndex;

    static int prevRoundfruitIndex;
    static int prevRoundspawnpointIndex;
    public static int spawnedFruitsCounter;
    // Use this for initialization
    void Start () {
        spawnedFruitsCounter = 0;
        fruits = GameObject.FindGameObjectsWithTag("fruit");
        spawnpoints = GameObject.FindGameObjectsWithTag("fruitSpawn");
        foreach (GameObject fruit in fruits) {
            fruit.SetActive(false);
        }
            spawnNewFruit();   
    }
	
	// Update is called once per frame
	void Update () {
      
    }

    static public void spawnNewFruit() {
     
        while (fruitIndex == prevRoundfruitIndex)
        {
            fruitIndex = Random.Range(0, fruits.Length);          
        }
        while (spawnpointIndex == prevRoundspawnpointIndex)
        {
            spawnpointIndex = Random.Range(0, spawnpoints.Length);
        }
            fruits[fruitIndex].transform.position = spawnpoints[spawnpointIndex].transform.position;

            fruits[fruitIndex].SetActive(true);
            fruits[fruitIndex].GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 0f, 0f);

            prevRoundfruitIndex = fruitIndex;
            prevRoundspawnpointIndex = spawnpointIndex;
    }

}
