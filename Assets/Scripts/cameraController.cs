﻿using UnityEngine;
using System.Collections;

public class cameraController : MonoBehaviour
{
    private PlayerControllerScript thePlayer;
    // Use this for initialization
    private GameObject floorParent;
    public bool cameraMovedToPlace;
    public float additionalDistanceFromPLayer;
    void Start()
    {
        thePlayer = FindObjectOfType<PlayerControllerScript>();        
        cameraMovedToPlace = false;
    }

    // Update is called once per frame


    void Update()
        
    {
        moveCameraUp();

        /*
          if (transform.position.y == (thePlayer.transform.position.y + additionalDistanceFromPLayer))
        {
            cameraMovedToPlace = true;
            thePlayer.onTopFloor = false;
        }
        else
        {
            cameraMovedToPlace = false;
        }

            if ( thePlayer.grounded && thePlayer.onTopFloor && transform.position.y <= thePlayer.transform.position.y + additionalDistanceFromPLayer) {
            
            float step = 2 * Time.deltaTime;                      

            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x,thePlayer.transform.position.y + additionalDistanceFromPLayer, transform.position.z), step);
            thePlayer.enabled = false;
        }
        */

    }
    public void moveCameraUp() {
        if (thePlayer.onTopFloor && transform.position.y <= thePlayer.collidingTumboPosition + additionalDistanceFromPLayer)
        {
            float step = 2 * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y + thePlayer.collidingTumboPosition, transform.position.z), step);
        }
            cameraMovedToPlace = true;
    }

    public void resetCamera() {
        transform.position = new Vector3(transform.position.x, thePlayer.transform.position.y, transform.position.z);
    }
}
