﻿using UnityEngine;
using System.Collections;

public class PlayerControllerScript : MonoBehaviour
{
    public float moveSpeed;
    public float jumpHeight;
    private Animator playerAnimator;
    public bool grounded;
    private Rigidbody2D myRigidbody;
    public Transform groundCheck;
    public LayerMask whatIsGround;
    public float groundCheckRadius;
    private bool facingRight;
    public GameManagerScript theGameManager;
    public bool onTopFloor;
    public cameraController cam;
    private bool useOnlyOnce = false;
    public sectionGeneratorScript sectionGen;
    public float collidingTumboPosition;
    public float jumpTimeCounter;
    public float jumpTime;
    // Use this for initialization
    void Start()
    {
        jumpTimeCounter = jumpTime;
        myRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        myRigidbody.freezeRotation = true;
        grounded = false;
        onTopFloor = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        playerAnimator.SetBool("isGrounded", grounded);
        playerAnimator.SetFloat("speed", Mathf.Abs(myRigidbody.velocity.x));

        if (grounded)
        {
            jumpTimeCounter = jumpTime;
        }

    }


    void OnBecameInvisible()
    {
        if (transform.position.y < 0)
        {
            //theGameManager.restartGame();
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        if (Input.GetKey(KeyCode.Space))
        {

            GetComponent<Rigidbody2D>().velocity = new Vector2(0, jumpHeight);
        }

        if (Input.GetKey(KeyCode.A))
        {
            flip(horizontal);
            GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            flip(horizontal);
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, 0);
        }


    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "topFloor")
        {
            GameObject[] topFloorTriggers = GameObject.FindGameObjectsWithTag("topFloor");
            for (var i = 0; i < topFloorTriggers.Length; i++)
                Destroy(topFloorTriggers[i]);

        }
        collidingTumboPosition = transform.position.y;
        sectionGen.generateSection();
        useOnlyOnce = true;
        onTopFloor = true;

    }
    

void OnTriggerExit2D(Collider2D other)
    {
        onTopFloor = false;
    }

    private void flip(float horizontal)
    {
        if (horizontal < 0 && !facingRight || horizontal > 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }
}